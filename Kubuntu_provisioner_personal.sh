#!/usr/bin/env bash

#echo -e 'Dpkg::Progress-Fancy "1";\nAPT::Color "1";' > /etc/apt/apt.conf.d/99fancyProgress

# alphabetize this list!

# update must run or all installations will fail if you're bootstrapping a system that is old by current standards.
sudo apt-get update
sudo apt-get dist-upgrade
# unattended-upgrade by default only upgrades security programs. that's what we want.
#sudo unattended-upgrade

# if VM created using Vagrant then Vagrant can manage your guest additions for you.
# Install Vbox tools to improve user experience; rebooting after this step will allow you to start using cut-and-paste between the host and guest
# apt install virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11 bridge-utils # There are three packages here! Don't forget virtualbox-guest-dkms!

sudo apt-get install -y --show-progress \
  aria2 \
  curl \
  falkon \
  git \
  htop \
  kate \
  net-tools \
  openssh-server \
  python-is-python3 \
  python3-pip \
  ripgrep \
  tmux \
  vim \
  vlc \
  wget

sudo apt-get install -y flatpak plasma-discover-backend-flatpak && flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak install -y one.ablaze.floorp net.mullvad.MullvadBrowser

#TODO copy personal settings files like vimrc falkon etc
mkdir -p $HOME/.aria2
cp aria2.conf ~/.aria2/
mkdir -p ~/.ssh
cp ssh_config ~/.ssh/config

curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
cp vimrc ~/.vimrc
#vim on mac already comes with desert, but selecting desert is the wrong desert.
vim +'PlugInstall --sync' +qa
echo "color desert" >> ~/.vimrc

# add KDE Plasma integration to firefox. only supported in Cosmic or greater
#   apt-get install -y plasma-browser-integration
#generic universal one-liner to download ff extension.  replace 'plasma-integration' with whatever shows up in the URL for your desired extension.  The sed part fixes slashes.
#   ff_plugin=`curl -L --compressed "https://addons.mozilla.org/en-US/firefox/addon/plasma-integration/?src=search" | sed "s^\\\u002F^/^g" | grep -E -o 'https:\/\/addons.mozilla.org\/firefox\/downloads\/file\/.*xpi'`
#   ff_plugin=${ff_plugin$$\?*}
#   aria2c -d /usr/share/mozilla/extensions/* ${ff_plugin}

#https://addons.mozilla.org/en-US/firefox/addon/noscript/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
#https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/

# change KDE settings from the commandline!
# find a setting in the GUI.  goto ~/.kde/share/config.  grep -rni [a useful word to locate which file has your setting].  look inside the file to find the group etc.
# no '--file' means global.  These all overwrite, yet notice that it's OK to start with a comma.
#  kwriteconfig5                   --group Layout    --key Options     ,terminate:ctrl_alt_bksp # fastest way to kill all gui apps
#  kwriteconfig5 --file kdeglobals --group General   --key ColorScheme 'Obsidian Coast'         # dark theme inside applications
#  kwriteconfig5 --file plasmarc   --group Theme     --key name        oxygen                   # dark theme for shell widgets
#  kwriteconfig5 --file oxygenrc   --group Windeco   --key ButtonSize  ButtonVeryLarge          # titlebar button size

