#!/usr/bin/env bash

set -x
echo -e 'Dpkg::Progress-Fancy "1";\nAPT::Color "1";' > /etc/apt/apt.conf.d/99fancyProgress
#special single install line to prepare for long downloads
export DEBIAN_FRONTEND=noninteractive
#update, autoremove, upgrade for when reprovisioning an existing machine
apt update
apt autoremove -y
apt upgrade -y
apt upgrade -y --show-progress aria2

#begin geolife dataset download in the background if not already downloaded. suppress output to this terminal.
(aria2c -R --conditional-get=true --log-level=notice -l /media/host_home/tracktable/geolife_`date +%Y_%m_%d_%H_%M_%S` --on-download-complete ./unzipper.sh -d /media/host_home/tracktable "https://download.microsoft.com/download/F/4/8/F4894AA5-FDBC-481E-9285-D5F8C4C4F039/Geolife%20Trajectories%201.3.zip" > /dev/null ) &
#begin Taxi dataset download
(aria2c -R --conditional-get=true --log-level=notice -l /media/host_home/tracktable/taxi_`date +%Y_%m_%d_%H_%M_%S` --on-download-complete ./unzipper.sh -d /media/host_home/tracktable "https://3ai2lq.dm.files.1drv.com/y4mQ0XafYE_yyZwTSFLc_2hQjXl3hzJqDRsMPrXHgIDYAfsoW0uBi_-_uGkrgmZmt1Q3VsBgtR0HBFg6D6crf3PJb-GWXtN-DlascezJGF6iJLcdX2YsuPG2PBchP3ITHLBLwwUYCJxP2nwbVXJlnUDkdUW4x4Ke2irc-KHkmOBd16EAQskgN_nEsmruhNsmWCaxaVoWr273RonzaH2-_rXZQ/T-drive%20Taxi%20Trajectories.zip?download&psid=1" > /dev/null ) &

# alphabetize this list!
# openshift and docker came from: https://wiki.centos.org/SpecialInterestGroup/PaaS/OpenShift-Quickstart so the remaining items are from that
# switches firefox to developer edition
#add-apt-repository -u -y ppa:ubuntu-mozilla-daily/firefox-aurora

#install / upgrade programs. idempotent. won't redo work. switched from one long list to individual commands in case some break. others will continue.
# aria is even better than curl
apt upgrade -y --show-progress aria2
apt upgrade -y --show-progress bridge-utils
apt upgrade -y --show-progress curl
apt upgrade -y --show-progress docker.io
apt upgrade -y --show-progress dos2unix
# web browser uses QT so less system resources than chrome or firefox
apt upgrade -y --show-progress falkon
apt upgrade -y --show-progress git
apt upgrade -y --show-progress htop
# text editor
apt upgrade -y --show-progress kate
apt upgrade -y --show-progress net-tools
apt upgrade -y --show-progress python3
apt upgrade -y --show-progress python3-pip
#python debugger that works with multiprocessing
apt upgrade -y --show-progress python3-pudb
# ripgrep actually does what egrep says it does. extended regular expressions. It's also the fastest grep ever.
apt upgrade -y --show-progress ripgrep
apt upgrade -y --show-progress tmux
apt upgrade -y --show-progress vim
apt upgrade -y --show-progress wget
apt upgrade -y --show-progress zsh

#not alphabetized. ordered for dependencies
#install python things for tracktable
#dependency for tracktable
pip3 install cartopy
#used in programs that use tracktable in order to programaticly elect point domain
pip3 install importlib
#seaborn is newer than matplotlib. it brings with it six, numpy, matplotlib, etc.
pip3 install seaborn
pip3 install numpy
pip3 install matplotlib
pip3 install pytest
#timezone stuff
pip3 install pyTZ
#shapely manipulates and analyses planar geometric objects. I believe shapely is only here for the geolife sample data set.
pip3 install shapely
#compatibility helpers for python 2 and 3
pip3 install six
#cartographic projections and coordinate transformations library
pip3 install pyproj
#C extensions for python
pip3 install cython
#needed by shapely
pip3 install geos
#read and write shapefiles
pip3 install pyshp
#sklearn is for PCA analysis to determine which features are most useful
pip3 install sklearn
pip3 install scipy
pip3 install tracktable
#excellent python editor. newest version found in pip rather than apt
pip3 install spyder

#edit /etc/sysconfig/docker file and add --insecure-registry 172.30.0.0/16 to the OPTIONS parameter.
sed -i '/OPTIONS=.*/c\OPTIONS="--selinux-enabled --insecure-registry 172.30.0.0/16"' /etc/sysconfig/docker
systemctl start docker 
systemctl enable docker

# make browser work just like from within your host OS https://www.happyassassin.net/2015/01/14/trusting-additional-cas-in-fedora-rhel-centos-dont-append-to-etcpkitlscertsca-bundle-crt-or-etcpkitlscert-pem/
#CERT URL provided by BAH help / support.  note:  dns doesn't work until after this is done, hence the 2nd try with ip address
#wget -t 2 -P /usr/share/ca-certificates/ http://ashbbcpsg01:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed
aria2c --console-log-level=warn -l /media/host_home/bah_cert_adding_log_`date +%Y_%m_%d_%H_%M_%S` --timeout=120 --max-file-not-found=1 --max-tries 2 -d /usr/share/ca-certificates/ http://ashbbcpsg01:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed || \
aria2c --console-log-level=warn -l /media/host_home/bah_cert_adding_log_`date +%Y_%m_%d_%H_%M_%S` -t 120 --max-file-not-found=1 -m 2 -d /usr/share/ca-certificates/ http://10.250.14.34:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed && \
update-ca-certificates

# add switchy omega to firefox to enable accessing "the IDE" which includes Jenkins
# ff_plugin=`curl -L --compressed "https://github.com/FelisCatus/SwitchyOmega/releases/latest" | grep -o 'href.*xpi'`
# ff_plugin=${ff_plugin#*\/}
# aria2c -d /usr/share/mozilla/extensions/* https://github.com/${ff_plugin}
#todo this doesn't work bc this script is run as root.  see if plugins can be installed globally
# mkdir home/vagrant/.mozilla/firefox/*.default/browser-extension-data/switchyomega@feliscatus.addons.mozilla.org
# cp $(dirname $0)/switchyomega_settings.js home/vagrant/.mozilla/firefox/*.default/browser-extension-data/switchyomega@feliscatus.addons.mozilla.org/storage.js

# add KDE Plasma integration to firefox. only supported in Cosmic or greater
apt install -y plasma-browser-integration
#generic universal one-liner to download ff extension.  replace 'plasma-integration' with whatever shows up in the URL for your desired extension.  The sed part fixes slashes.
ff_plugin=`curl -L --compressed "https://addons.mozilla.org/en-US/firefox/addon/plasma-integration/?src=search" | sed "s^\\\u002F^/^g" | rg -o 'https://addons.mozilla.org/firefox/downloads/file/.*xpi'`
#  remove trailing characters from string
ff_plugin=${ff_plugin%%\?*}
aria2c -R --conditional-get=true --log-level=notice  -l /media/host_home/plasma-browser-integration_log_`date +%Y_%m_%d_%H_%M_%S` -d /usr/share/mozilla/extensions/* ${ff_plugin}

# change KDE settings from the commandline!
# find a setting in the GUI.  goto ~/.kde/share/config.  grep -rni [a useful word to locate which file has your setting].  look inside the file to find the group etc.
# no '--file' means global.  These all overwrite, yet notice that it's OK to start with a comma.
kwriteconfig5                   --group Layout    --key Options     ,terminate:ctrl_alt_bksp # fastest way to kill all gui apps
kwriteconfig5 --file kdeglobals --group General   --key ColorScheme 'Obsidian Coast'         # dark theme inside applications
kwriteconfig5 --file plasmarc   --group Theme     --key name        oxygen                   # dark theme for shell widgets
kwriteconfig5 --file oxygenrc   --group Windeco   --key ButtonSize  ButtonVeryLarge          # titlebar button size

set +x

