#!/usr/bin/env bash
#installs p2pool
#configures 
#starts 

#run from git repo folder.
here_dir=`pwd`

*****************
#set up p2pool
*****************
mkdir ~/p2pool 2>/dev/null
cd ~/p2pool

set -x
aria2c -c -d . https://p2pool.io/SChernykh.asc

diff -qs <( gpg --show-keys --with-fingerprint --keyid-format=long SChernykh.asc | rg --pcre2 -o "(?<=Key fingerprint = ).*" ) <(echo "1FCA AB4D 3DC3 310D 16CB  D508 C47F 82B5 4DA8 7ADF") || exit 1

gpg --import SChernykh.asc

aria2c -c -d . https://github.com/SChernykh/p2pool/releases/download/v4.3/sha256sums.txt.asc

gpg --verify sha256sums.txt.asc || exit 1

aria2c -c -d . https://github.com/SChernykh/p2pool/releases/download/v4.3/p2pool-v4.3-linux-x64.tar.gz

rg -A2 p2pool-v4.3-linux-x64.tar.gz sha256sums.txt.asc | tee >( rg --pcre2 -o "(?<=SHA256: ).*" > p2pool_correct_sha256 )

shasum -a 256 p2pool-v4.3-linux-x64.tar.gz | cut -d' ' -f1 | tee p2pool_bin_sha256

diff -iqs p2pool_correct_sha256 p2pool_bin_sha256 || exit 1

tar -xvzf p2pool-v4.3-linux-x64.tar.gz 
sudo cp -r p2pool-v4.3-linux-x64 /home/monero/
sudo chown -R monero:monero /home/monero/p2pool-v4.3-linux-x64

set +x
#must use address of the monero node that you set up (monerod)
#must be in a folder that monero user can write to
#monero user defaults to sh instead of bash
#sudo su
#su monero
#./p2pool-v4.3-linux-x64/p2pool --host 127.0.0.1 --wallet YOUR_WALLET_ADDRESS
