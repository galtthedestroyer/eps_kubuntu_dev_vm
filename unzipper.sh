#!/usr/bin/env bash

# -aa forces all files to be treated as text and converts them to unix format.  
#-n don't duplicate in case we've already downloaded
unzip -aa -n "$3" -x "*.pdf" > unzip_log_`date +%Y_%m_%d_%H_%M_%S`
#now unzip pdf files as binary
unzip -n "$3" "*.pdf" > unzip_log_`date +%Y_%m_%d_%H_%M_%S`

