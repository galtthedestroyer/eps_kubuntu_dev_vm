#!/usr/bin/env bash
# adds plugins to Firefox based browsers.
# currently supports Floorp and Firefox.
# usage: firefox_provisioner.sh <browsername>
# where browsername must be lowercase:
# floorp
# firefox

#todo
#https://www.wikihow.com/Install-Firefox-Extensions
#found out that I can download an xpi file
#from commandline run firefox path_to_xpi
#firefox will open with a popup to ask to install it. (same as when clicking in the webstore.)
#have the script wait for the firefox process to end. 
#repeat with other xpi files.
#on osx: open -a "Firefox" "path_to_xpi_file" #quotes are necessary.

#todo osx open command doesn't create a pid
echo "untested. see todo"
exit

PLUGINS=("noscript" "ublock_origin")
case `uname` in
  "Darwin")
    RUN_BROWSER=("open" "-a" "$1")
    ;;
  "Linux")
#    I always use KDE Plasma desktop environment.
    PLUGINS+=("plasma_integration")
    RUN_BROWSER=("$1")
    ;;
  *)
    echo "todo: this OS"
    ;;
esac

#generic universal one-liner to download ff extension.  replace '${PLUGIN}' with whatever shows up in the URL for your desired extension.  The sed part fixes slashes.
for PLUGIN in "${PLUGINS[@]}"; do
  ff_plugin=`curl -L --compressed "https://addons.mozilla.org/en-US/firefox/addon/${PLUGIN}/?src=search" | sed "s^\\\u002F^/^g" | grep -E -o 'https:\/\/addons.mozilla.org\/firefox\/downloads\/file\/.*?xpi' | head -n1`
#  ff_plugin=${ff_plugin$$\?*}
  aria2c -d "${HOME}/Downloads" ${ff_plugin}
done

cd $HOME/Downloads

XPIS=`find $HOME/Downloads -name "*.xpi"`
for XPI in "${XPIS[@]}"; do
  "${RUN_BROWSER[@]} \"$XPI\""
  forFF=$!
  wait forFF
done

#    apt-get install -y plasma-browser-integration

