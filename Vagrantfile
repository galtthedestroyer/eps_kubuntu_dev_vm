# vi: set ft=ruby :

#virtual disk too small?  (if the box uses vmdk image it can't be resized.)  3 options:
#1 add an empty image to use as '/home' so that your data doesn't get clobbered if you want to kill your main VM.
#2 glue on another empty image using LVM.  It will have to be reformatted if you want to redo your VM.
#3 convert to vdi and resize.  resize the vdi, boot from a live iso, grow the partition, resize the fs.

# get current users home directory for syncing to guest vm.  https://github.com/projectatomic/adb-atomic-developer-bundle/issues/336
# maybe use this test instead: if Vagrant::Util::Platform.windows?
@os = RbConfig::CONFIG['host_os']
	case
		when @os.downcase.include?('mswin') | @os.downcase.include?('mingw') | @os.downcase.include?('cygwin')
			homedir= ENV['USERPROFILE']
		when @os.downcase.include?('darwin') | @os.downcase.include?('linux')
			homedir = "~"
		else
			puts 'You are not on a supported platform. exiting...'
			print "unknown os: \"" + @os + "\". Please email this error to bl...@blah.com"
			exit
	end

global_name = "Kubuntu_22_04_dev_environment"

# All Vagrant configuration is done below. The "2" in Vagrant.configure configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what you're doing.
Vagrant.configure("2") do |config|
  # Every Vagrant development environment requires a box. You can search for boxes at https://vagrantcloud.com/search.
  #config.vm.box_download_insecure = true #On the bah network I had to use this once, so here it is permanently.
  config.vm.box = "buzzthestrawman/kubuntu22"

  # Disable automatic box update checking. If you disable this, then boxes will only be checked for updates when the user runs `vagrant box outdated`. This is not recommended.
  # Unfortunately, if the box gets updated the guest additions get blown away.  When they're re-added things might break.  There is no undo feature for updating boxes.
   config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine. NOTE: This will enable public access to the opened port
  config.vm.network "forwarded_port", guest: 22, host: 2222

  # Create a forwarded port mapping which allows access to a specific port within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third argument is a set of non-required options.
    config.vm.synced_folder homedir, "/media/host_home"

    # note: multiple providers can be configured here.  e.g hyperV for any triggers that are provider specific, move them into their provider's code block
    # multiple proviers in the file means that a default provider must be set overall for vagrant or one must be specified at each instance of "vagrant up"
  #
    config.vm.provider "virtualbox" do |vb|
        vb.name = global_name
  #   # Display the vm's GUI when booting the machine. note:  still comes up with terminal if guest OS not set to boot to gui.
        vb.gui = true
  #
  #   # All of these [modifyvm] must be done while not vm running:
        vb.memory      = "16384"
        vb.cpus        = "4"
        vb.customize ["modifyvm" , :id, "--clipboard-mode", "bidirectional"]
        vb.customize ["modifyvm" , :id, "--draganddrop"   , "bidirectional"]
        vb.customize ["modifyvm" , :id, "--mouse"         , "usbmultitouch"]
        vb.customize ["modifyvm" , :id, "--usbxhci"       , "off"]            #usb3 not available without $$ for extensions
        vb.customize ["modifyvm" , :id, "--usbehci"       , "off"]            #usb3 not available without $$ for extensions
        vb.customize ["modifyvm" , :id, "--usbohci"       , "on"]             #usb3 not available without $$ for extensions
        vb.customize ["modifyvm" , :id, "--vram"          , "128"]            #necessary for easy desktop resolution changes
        #vb.customize ["storageattach", :id, "--storagectl", "IDE Controller", "--port", "0", "--device", "1", "--type", "dvddrive", "--medium", "emptydrive"]  
    end

    # All of these [controlvm] must be done while vm running.
    # https://www.virtualbox.org/manual/ch08.html#vboxmanage-controlvm
    # this trigger doesn't work 2018-08-21.  vagrant bug: https://github.com/hashicorp/vagrant/issues/10099
  	# to get a proper resolution.  maximize host window.  view -> scaled mode.  then set resolution within guest normally.
    config.trigger.after :up do |trigger|
      trigger.name = "Video Mode Hinter"
      trigger.info = "Trying to set the video mode to 1920 x 1080 x 32"
      trigger.run = {inline: "VBoxManage controlvm " + global_name + " setvideomodehint 1920 1080 32"}
    end

    config.trigger.after :provision do |trigger|
      trigger.info = "Enabling fullscreen video for this vm."
      trigger.run = {inline: "VBoxManage setextradata " + global_name + " GUI/Fullscreen on"}
    end

  # Enable provisioning with a shell script. Additional provisioners such as Puppet, Chef, Ansible, Salt, and Docker are also available.
  # Please see the documentation for more information about their specific syntax and use.

  # ex of setting env vars regardless of which shell is used.
  config.vm.provision "shell", path: "Kubuntu_provisioner.sh" #, env: {"ff_plugin" => "2.5.20"}, inline: <<-SHELL
end
