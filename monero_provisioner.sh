#!/usr/bin/env bash
#installs monerod, monero wallet
#configures monerod
#starts monerod

#run from git repo folder.
here_dir=`pwd`

*****************
#set up monerod, wallet
*****************
mkdir ~/monero 2>/dev/null
cd ~/monero

aria2c -c -d . https://raw.githubusercontent.com/monero-project/monero/master/utils/gpg_keys/binaryfate.asc

diff -qs <( gpg --show-keys --with-fingerprint --keyid-format=long binaryfate.asc | rg --pcre2 -o "(?<=Key fingerprint = ).*" ) <(echo "81AC 591F E9C4 B65C 5806  AFC3 F0AF 4D46 2A0B DF92") || exit 1

gpg --import binaryfate.asc

aria2c -c -d . https://www.getmonero.org/downloads/hashes.txt

gpg --verify hashes.txt || exit 1

aria2c -c -d . https://downloads.getmonero.org/cli/linux64

shasum -a 256 monero-linux-x64-*.bz2

rg -N monero-linux-x64- hashes.txt

diff -qs <( shasum -a 256 monero-linux-x64-*.bz2 ) <( rg -N monero-linux-x64- hashes.txt ) || exit 1

tar -xvjf monero-linux-x64-*.bz2
sudo cp monero-x86_64-linux-gnu-v0.18.3.4/* /usr/local/bin/.
cp $here_dir/bitmonero.conf ~/.bitmonero/

#set before running monerod.
#DNS_PUBLIC="tcp://9.9.9.9"
#./monerod --zmq-pub tcp://127.0.0.1:18083 --out-peers 32 --in-peers 64 --add-priority-node=p2pmd.xmrvsbeast.com:18080 --add-priority-node=nodes.hashvault.pro:18080 --disable-dns-checkpoints --enable-dns-blocklist
#all settings moved to conf file.
#run as self. no big deal.
monerod 


#systemctl daemon-reload
#systemctl enable monerod
#sudo mkdir -p /etc/monero     # config

#couldn't get service to work
#sudo useradd --system monero
#sudo chown monero:monero /usr/local/bin/monero*
#sudo mkdir /run/monero
#sudo mkdir -p /var/lib/monero # blockchain
#sudo mkdir -p /var/log/monero # logs
#
#sudo cp $here_dir/monerod.conf /etc/monero/
#sudo cp $here_dir/monerod.service /etc/systemd/system/
#sudo chown -R monero:monero /etc/monero
#sudo chown -R monero:monero /var/lib/monero
#sudo chown -R monero:monero /var/log/monero
#
#systemctl restart monerod
