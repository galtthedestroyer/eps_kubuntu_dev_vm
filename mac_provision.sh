#!/usr/bin/env zsh
#keep zsh. see below

#TODO possibly switch to use ansible for all of this. ansible installs via pip. 
#I'd have to install python, pip, ansible. 
#homebrew, python, and xcode install would still be done separately. It's NOT worth it
#bc any python installed by homebrew should NOT be used by the user.
#modules: osx_defaults, homebrew

#this script is divided into 3 sections:
#1. all macs including personal
#2. DOD things
#3. DIA things

#1. all macs including personal
#install command line tools for xcode or else your homebrew install will fail.
#this will launch gui things
xcode-select --install

#install homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
#run the two commands that the install script tells you to run
(echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> ~/.zprofile
#the eval brew shellenv command in zprofile doesn't work
echo 'export PATH="/opt/homebrew/bin:$PATH"' >> ~/.zprofile
source ~/.zprofile

#Apple has horribly outdated bash because bash switched to GPL3
brew install bash
sudo sh -c 'echo /opt/homebrew/bin/bash >> /etc/shells'
cat <<EOF >> ~/.bashrc
export PATH="/opt/homebrew/bin:$PATH"
alias python=python3
alias pip=pip3
XDG_CONFIG_HOME=$HOME
EOF
#apparently chsh is not permanent. my shell reverted to zsh somehow.
chsh -s /opt/homebrew/bin/bash

#brew install ansible
brew install gawk
#brew install --cask google-chrome
#brew install --cask chromium --no-quarantine
#file manager
brew install --cask spacedrive
#virtualization based on Qemu
#brew install --cask utm
#for remotely connecting via Citrix
#brew install --cask citrix-workspace
#best grep
brew install ripgrep-all
#best download manager
brew install aria2c
#cool terminal emulator
brew install iterm2
cp iterm2_profile.json ~/Library/Application\ Support/iTerm2/DynamicProfiles/
cp iterm2_set_default.py ~/Library/Application\ Support/iTerm2/Scripts/

#set up app preferences
mkdir -p $HOME/.aria2
cp aria2.conf ~/.aria2/
cp ssh_config ~/.ssh/config

##Vim
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
cp vimrc ~/.vimrc
vim +'PlugInstall --sync' +qa
#vim on mac already comes with desert as default, but explicitly selecting desert is the wrong desert. ¯\_(ツ)_/¯

##Finder

defaults write $HOME/Library/Preferences/com.apple.finder.plist ShowPathBar   -bool TRUE && killall Finder
defaults write $HOME/Library/Preferences/com.apple.finder.plist ShowStatusBar -bool TRUE && killall Finder
/usr/libexec/PlistBuddy -c "set :SearchRecentsViewSettings:WindowState:ShowToolbar true" $HOME/Library/Preferences/com.apple.finder.plist && killall Finder

##Terminal white on black, no transparency. big font and large window size are a pain to set.

defaults write com.apple.Terminal "Default Window Settings" "Pro"
defaults write com.apple.Terminal "Startup Window Settings" "Pro"
plutil -replace 'Window Settings'.Pro.BackgroundColor -data "YnBsaXN0MDDUAQIDBAUGBwpYJHZlcnNpb25ZJGFyY2hpdmVyVCR0b3BYJG9iamVjdHMSAAGGoF8QD05TS2V5ZWRBcmNoaXZlctEICVRyb290gAGjCwwTVSRudWxs0w0ODxAREldOU1doaXRlXE5TQ29sb3JTcGFjZVYkY2xhc3NCMAAQA4AC0hQVFhdaJGNsYXNzbmFtZVgkY2xhc3Nlc1dOU0NvbG9yohYYWE5TT2JqZWN0CBEaJCkyN0lMUVNXXWRseYCDhYeMl6CoqwAAAAAAAAEBAAAAAAAAABkAAAAAAAAAAAAAAAAAAAC0" ~/Library/Preferences/com.apple.Terminal.plist 

##global
defaults write .GlobalPreferences AppleInterfaceStyle Dark

##enable ssh to this machine
### requires that "this" terminal program has "full disk access" which can't be granted from a terminal. A script can't self-grant anyway.
### search spotlight or system settings for "full disk access". enable your terminal program.
sudo systemsetup -setremotelogin on
#otherwise, enable remote login using system settings.

#2. DOD things
#get certs for remoting in to DOD servers
cd ~/Downloads
aria2c https://dl.dod.cyber.mil/wp-content/uploads/pki-pke/zip/unclass-certificates_pkcs7_DoD.zip
unzip unclass-certificates_pkcs7_DoD.zip
#DIA Remote User Guide v 1.3 doesn't work for Mac. add these certs also:
aria2c https://dl.dod.cyber.mil/wp-content/uploads/pki-pke/zip/unclass-dod_approved_external_pkis_trust_chains.zip
unzip unclass-dod_approved_external_pkis_trust_chains.zip "*/Intermediate_and_Issuing_CA_Certs/*"

#install certs
#shell-fu doesn't work. use gui keychain app
echo "add the certificates manually from finder"

#zipinfo used so we don't need to determine if the zip file is nested.
#some suggest 'trustRoot' works for some cert types.
#some suggest -d option for der formatted files.
#zipinfo -1 unclass-certificates_pkcs7_DoD.zip | grep "\.p.*" | xargs -I % sudo security add-trusted-cert -r trustAsRoot -k ~/Library/Keychains/login.keychain-db %
#some say -d also implies that the cert is always CA so it should go into system keychain. that didn't work either.
#sudo security add-trusted-cert -d -r trustRoot -k "/Library/Keychains/System.keychain" "path/to/your_certificate.der"

#CAC
#https://www.ndu.edu/Incoming-Students/Cybersecurity-Compliance/Apple-Support-Enabling-CAC/
defaults read /Library/Preferences/com.apple.security.smartcard DisabledTokens 
echo "should say does not exist"

#3. DIA things
#DIA Talos program
#Azure virtual desktop AVD
#program renamed to Windows App so we might have to change the brew command soon.
brew install microsoft-remote-desktop
brew install microsoft-teams

