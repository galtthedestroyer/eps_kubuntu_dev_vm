{
  "policy": {
    "DEFAULT": {
      "capabilities": [
        "frame",
        "fetch",
        "noscript",
        "other"
      ],
      "temp": false
    },
    "TRUSTED": {
      "capabilities": [
        "script",
        "object",
        "media",
        "frame",
        "font",
        "webgl",
        "fetch",
        "ping",
        "noscript",
        "lazy_load",
        "unchecked_css",
        "lan",
        "other"
      ],
      "temp": false
    },
    "UNTRUSTED": {
      "capabilities": [],
      "temp": false
    },
    "sites": {
      "trusted": [
        "§:addons.mozilla.org",
        "§:afx.ms",
        "§:ajax.aspnetcdn.com",
        "§:bootstrapcdn.com",
        "§:code.jquery.com",
        "§:firstdata.com",
        "§:firstdata.lv",
        "§:gfx.ms",
        "§:google.com",
        "§:googlevideo.com",
        "§:gstatic.com",
        "§:hotmail.com",
        "§:live.com",
        "§:live.net",
        "§:maps.googleapis.com",
        "§:mozilla.net",
        "§:netflix.com",
        "§:nflxext.com",
        "§:nflximg.com",
        "§:nflxvideo.net",
        "§:noscript.net",
        "§:outlook.com",
        "§:passport.com",
        "§:passport.net",
        "§:passportimages.com",
        "§:paypal.com",
        "§:paypalobjects.com",
        "§:securecode.com",
        "§:securesuite.net",
        "§:sfx.ms",
        "§:tinymce.cachefly.net",
        "§:wlxrs.com",
        "§:yahoo.com",
        "§:yahooapis.com",
        "§:yimg.com",
        "§:youtube.com",
        "§:ytimg.com",
        "§:github.com",
        "§:githubassets.com",
        "§:irs.gov",
        "§:id.me",
        "§:cigna.com",
        "§:amazon.com",
        "§:ssl-images-amazon.com",
        "§:media-amazon.com",
        "§:hsabank.com",
        "§:fontawesome.com",
        "§:siege-amazon.com",
        "§:reddit.com",
        "§:redditstatic.com",
        "§:yubico.com",
        "§:a2z.com",
        "§:aiv-cdn.net",
        "§:redfin.com",
        "§:cdn-redfin.com",
        "§:proton.me",
        "§:adp.com",
        "§:microsoftonline.com",
        "§:msauth.net",
        "§:msftauth.net",
        "§:microsoft.com",
        "§:azure.com",
        "§:sharepoint.com",
        "§:office.net",
        "googleapis.com",
        "§:oktacdn.com",
        "§:lh1ondemand.com",
        "§:uhc.com",
        "§:healthsafe-id.com",
        "§:chase.com",
        "§:chasecdn.com",
        "§:doxy.me"
      ],
      "untrusted": [],
      "custom": {
        "https://s3-iad-2.cf.trailer.row.aiv-cdn.net": {
          "capabilities": [
            "frame",
            "fetch",
            "noscript",
            "other",
            "media"
          ],
          "temp": false
        },
        "§:ajax.googleapis.com": {
          "capabilities": [
            "script",
            "object",
            "media",
            "frame",
            "font",
            "webgl",
            "fetch",
            "ping",
            "noscript",
            "lazy_load",
            "unchecked_css",
            "lan",
            "other"
          ],
          "temp": false
        },
        "§:cloudflare.com": {
          "capabilities": [
            "frame",
            "fetch",
            "noscript",
            "other"
          ],
          "temp": false
        },
        "§:clearancejobs.com": {
          "capabilities": [
            "frame",
            "fetch",
            "noscript",
            "other",
            "script"
          ],
          "contextual": {
            "§:clearancejobs.com": {
              "capabilities": [
                "frame",
                "fetch",
                "noscript",
                "other",
                "script"
              ],
              "temp": false
            }
          },
          "temp": false
        },
        "§:indeed.com": {
          "capabilities": [
            "frame",
            "fetch",
            "noscript",
            "other",
            "script"
          ],
          "contextual": {
            "§:indeed.com": {
              "capabilities": [
                "frame",
                "fetch",
                "noscript",
                "other",
                "script"
              ],
              "temp": false
            }
          },
          "temp": false
        },
        "§:ipqscdn.com": {
          "capabilities": [
            "frame",
            "fetch",
            "noscript",
            "other",
            "script"
          ],
          "contextual": {
            "§:indeed.com": {
              "capabilities": [
                "frame",
                "fetch",
                "noscript",
                "other",
                "script"
              ],
              "temp": false
            }
          },
          "temp": false
        },
        "§:gigabyte.com": {
          "capabilities": [
            "frame",
            "fetch",
            "noscript",
            "other",
            "script"
          ],
          "contextual": {
            "§:gigabyte.com": {
              "capabilities": [
                "frame",
                "fetch",
                "noscript",
                "other",
                "script"
              ],
              "temp": false
            }
          },
          "temp": false
        },
        "§:chasebonus.com": {
          "capabilities": [
            "frame",
            "fetch",
            "noscript",
            "other",
            "script"
          ],
          "contextual": {
            "§:chasebonus.com": {
              "capabilities": [
                "frame",
                "fetch",
                "noscript",
                "other",
                "script"
              ],
              "temp": false
            }
          },
          "temp": false
        },
        "§:gigabyte.com.tw": {
          "capabilities": [
            "frame",
            "fetch",
            "noscript",
            "other",
            "script"
          ],
          "contextual": {
            "§:gigabyte.com": {
              "capabilities": [
                "frame",
                "fetch",
                "noscript",
                "other",
                "script"
              ],
              "temp": false
            }
          },
          "temp": false
        }
      }
    },
    "enforced": true,
    "autoAllowTop": false
  },
  "local": {
    "debug": false,
    "showCtxMenuItem": true,
    "showCountBadge": true,
    "showFullAddresses": false,
    "showProbePlaceholders": true,
    "amnesticUpdates": false,
    "storage": "local",
    "uuid": "edff4c0b-273b-4a57-9c4a-258168f7ecfa"
  },
  "sync": {
    "global": false,
    "xss": true,
    "TabGuardMode": "incognito",
    "TabGuardPrompt": "post",
    "cascadeRestrictions": false,
    "overrideTorBrowserPolicy": false,
    "storage": "sync"
  },
  "xssUserChoices": {}
}