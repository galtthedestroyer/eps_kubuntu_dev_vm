#get a list of VMs
vboxmanage list vms

#get the UUID of the smart card reader
vboxmanage list usbhost | rg -e "martcard" -e "UUID" | rg -B 1 "martcard"

#share smartcard reader with running guest
#example vboxmanage controlvm Kubuntu_dev_environment usbatach 025eb79b-3efc-4c6d-9728-a0c5c5c00a1f
echo "eg: vboxmanage controlvm Kubuntu_dev_environment usbatach 025eb79b-3efc-4c6d-9728-a0c5c5c00a1f"
echo "detach command is detach"
