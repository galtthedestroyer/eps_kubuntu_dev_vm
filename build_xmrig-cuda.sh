#!/usr/bin/env bash

#requires nvidia drivers already installed.

cd ..
git clone https://github.com/xmrig/xmrig-cuda.git
cd xmrig-cuda
mkdir build
cd build || exit 1
cmake .. -DCUDA_LIB=/usr/local/cuda/lib64/stubs/libcuda.so -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda
make -j$(nproc)

