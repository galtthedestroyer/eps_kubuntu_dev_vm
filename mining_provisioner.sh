#!/usr/bin/env bash

#run from git repo folder.
here_dir=`pwd`

#wget https://developer.download.nvidia.com/compute/cuda/12.6.3/local_installers/cuda_12.6.3_560.35.05_linux.run &

sudo apt-get install -y xmrig &

#for xmrig-cuda
lspci | rg -i nvidia && sudo apt-get install -y cmake nvidia-cuda-toolkit && sudo ubuntu-drivers install

#https://github.com/SChernykh/p2pool?tab=readme-ov-file#default-p2pool-parameters
#Prepare enough huge pages (each of monerod/P2Pool/XMRig needs them):
sudo sysctl vm.nr_hugepages=3072

#It is highly recommended that you create a separate restricted user account (in your OS) for mining.
sudo useradd --system --create-home monero
#allow all users in monero group to become monero user without password
sudo sed -Ei '/^auth[[:blank:]]*sufficient pam_rootok.so$/aauth       [success=ignore default=1] pam_succeed_if.so user = monero\nauth       sufficient   pam_succeed_if.so use_uid user ingroup monero' /etc/pam.d/su
#add yourself to monero group
sudo usermod -aG monero $USER
#amd drivers create a render group that owns the renderers (graphics cards?)
#add monero user to render group
sudo usermod -aG render monero

#for amd
#download proper amdgpu-install package. for cards older than vega 10 must use version 5.7.1 or older.
#run it.
#uncomment the repo in:
#/etc/apt/sources.list.d/amdgpu-proprietary.list

#for graphics cards older than vega 10 opencl must be legacy
#usecase opencl doesn't install any graphics stuff. only opencl.
#amdgpu-install --usecase=opencl --opencl=legacy

#in order:
# if monerod and p2pool haven't already been set up on a machine:
# monero_provisioner.sh
# make a new wallet by running the wallet program with no parameters.
# get wallet public address.
# p2pool_provisioner.sh
#
# both monerod and p2pool run once per "house/location" so keep 127...
# see p2pool_provisioner for correct command
# sudo -u monero p2pool --host 127.0.0.1 --wallet YOUR_WALLET_ADDRESS
#
# else #for any additional mining rig
# get ip address of monero/p2pool node from mikrotik
# xmrig -o YOUR_P2POOL_NODE_IP:3333

#sudo -u monero  xmrig --opencl --cuda -o 127.0.0.1:3333 -u walletaddress -p optionalpoolpassword -k

