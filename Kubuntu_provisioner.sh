#!/usr/bin/env bash

echo -e 'Dpkg::Progress-Fancy "1";\nAPT::Color "1";' > /etc/apt/apt.conf.d/99fancyProgress

# alphabetize this list!

# update must run or all installations will fail if you're bootstrapping a system that is old by current standards.
apt update
#apt upgrade
# unattended-upgrade by default only upgrades security programs. that's what we want.
unattended-upgrade

# if VM created using Vagrant then Vagrant can manage your guest additions for you.
# Install Vbox tools to improve user experience; rebooting after this step will allow you to start using cut-and-paste between the host and guest
# apt install virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11 bridge-utils # There are three packages here! Don't forget virtualbox-guest-dkms!

apt install -y --show-progress \
  aria2 \
  curl \
  falkon \
  git \
  htop \
  kate \
  net-tools \
  python-is-python3 \
  python3-pip \
  ripgrep \
  #screen \
  tmux \
  vim \
  wget \
  zsh

#while within a booz network.  current as of 2019.  might be outdated.
#edit /etc/sysconfig/docker file and add --insecure-registry 172.30.0.0/16 to the OPTIONS parameter.
#sed -i '/OPTIONS=.*/c\OPTIONS="--selinux-enabled --insecure-registry 172.30.0.0/16"' /etc/sysconfig/docker
systemctl is-active docker
systemctl enable docker

# make firefox work just like from within your host OS https://www.happyassassin.net/2015/01/14/trusting-additional-cas-in-fedora-rhel-centos-dont-append-to-etcpkitlscertsca-bundle-crt-or-etcpkitlscert-pem/
#CERT URL provided by BAH help / support.  note:  dns doesn't work until after this is done, hence the 2nd try with ip address
#wget -t 2 -P /usr/share/ca-certificates/ http://ashbbcpsg01:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed
#aria2c --max-file-not-found=1 -m 2 -d /usr/share/ca-certificates/ http://ashbbcpsg01:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed || \
#aria2c --max-file-not-found=1 -m 2 -d /usr/share/ca-certificates/ http://10.250.14.34:8081/SSL/Download_ca/keyring/SSL_MSPKI_Signed
#update-ca-certificates

# add switchy omega to firefox to enable accessing "the IDE" which includes Jenkins
# ff_plugin=`curl -L --compressed "https://github.com/FelisCatus/SwitchyOmega/releases/latest" | grep -o 'href.*xpi'`
# ff_plugin=${ff_plugin#*\/}
# aria2c -d /usr/share/mozilla/extensions/* https://github.com/${ff_plugin}
#todo this doesn't work bc this script is run as root.  see if plugins can be installed globally
# mkdir home/vagrant/.mozilla/firefox/*.default/browser-extension-data/switchyomega@feliscatus.addons.mozilla.org
# cp $(dirname $0)/switchyomega_settings.js home/vagrant/.mozilla/firefox/*.default/browser-extension-data/switchyomega@feliscatus.addons.mozilla.org/storage.js

# add KDE Plasma integration to firefox. only supported in Cosmic or greater
#   apt-get install -y plasma-browser-integration
#generic universal one-liner to download ff extension.  replace 'plasma-integration' with whatever shows up in the URL for your desired extension.  The sed part fixes slashes.
#   ff_plugin=`curl -L --compressed "https://addons.mozilla.org/en-US/firefox/addon/plasma-integration/?src=search" | sed "s^\\\u002F^/^g" | grep -E -o 'https:\/\/addons.mozilla.org\/firefox\/downloads\/file\/.*xpi'`
#   ff_plugin=${ff_plugin$$\?*}
#   aria2c -d /usr/share/mozilla/extensions/* ${ff_plugin}

# change KDE settings from the commandline!
# find a setting in the GUI.  goto ~/.kde/share/config.  grep -rni [a useful word to locate which file has your setting].  look inside the file to find the group etc.
# no '--file' means global.  These all overwrite, yet notice that it's OK to start with a comma.
#  kwriteconfig5                   --group Layout    --key Options     ,terminate:ctrl_alt_bksp # fastest way to kill all gui apps
#  kwriteconfig5 --file kdeglobals --group General   --key ColorScheme 'Obsidian Coast'         # dark theme inside applications
#  kwriteconfig5 --file plasmarc   --group Theme     --key name        oxygen                   # dark theme for shell widgets
#  kwriteconfig5 --file oxygenrc   --group Windeco   --key ButtonSize  ButtonVeryLarge          # titlebar button size

