#useful printer commands
#query queues
lpq
lpinfo
#list all known printers and indicate default
lpstat -p -d
#list all known printer names only
lpstat -p | rg 'printer (\S*)' -or '$1'
#administer
lpadmin
#enable a printer
lpadmin -p printername -E
#enable all printers
lpstat -p | rg 'printer (\S*)' -or '$1' | xargs -I % lpadmin -p % -E

